
## First download "source_code" folder.

# If you already have installed python 3.12:

On Linux run "start.sh" inside the folder.

(Basically one need to create virtual environment)


# If you do NOT have installed python 3.12:

# step 1
1) download and install anaconda from https://www.anaconda.com/
2) launch anaconda
3) go "environments" tab
4) click "create" 
5) name it somehow (for ex. "SYNCmoss")
6) select python 3.12.7
7) wait until it will be created
8) click on your new environment ("SYNCmoss")
9) click "play" button near environment name 
10) select "open terminal"

# step 2
in opened terminal you need to enter following commands:

```bash
pip install kivy
pip install plyer
pip install pyobjus # (required only for MacOS)
pip install numba
pip install matplotlib
pip install scipy 
```

# step 3
now environment is ready, and you need to run software
in the same terminal go to folder with downloaded code for software by command 'cd'
for example
```
cd software/syncmoss/ # in Windows instead each "/" use "\".
```
then do 
```
python3 prog_raw.py
```

# step ...
If everything is installed, to run software you need to repeat
substeps 2,3,8,9,10 from "step 1" and then do "step 3"

### Please be careful the software is not tested on MacOS by author. Could contain bugs, could look strange... Author only checked that it could run, but without any beta-test. However, several users said that it works through anaconda nicely, only with small interface "distortions".
 






