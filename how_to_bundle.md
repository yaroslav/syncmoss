
This guide assumes that you are using your computer on Linux with full accesses (sudo commands),
in other cases you can try to follow from step 2.


# 1. Check python version:
In your terminal run the following command:

```bash
python3 -V
```

Output will contain your installed python version if it exists.
If python version is not 3.12 go through the next 2 steps.

## 1.1. Install python of proper version
Install python 3.12.7:

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install python3.12
```

## 1.2. Select installed python
To select an appropriate python version as a default one you should use theses commands:

```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.12 0
sudo update-alternatives --config python3
# select currently installed python by specifying its number '1' / '2' / '3' ...
```
If you do not have "pip" installed run also these commands:
```bash
sudo apt install python3-pip
pip install --upgrade pip
```
# 2. Create environment to bundle/use


 Go to folder with the code. For example:
```
cd software/syncmoss/
```
Create and activate virtual environment:

```bash
python3 -m venv syncmoss_evn # if failed do before 'sudo apt-get install python3-venv' and repeat again
source syncmoss_env/bin/activate
```

install libraries:

```bash
pip install kivy
pip install plyer
pip install pyobjus # (required only for MacOS)
pip install numba
pip install matplotlib
pip install scipy
```
if "pip install" commands failed try
```bash
pip install --upgrade pip
```
and then repeat "pip install" commands.

If you want to bundle you will need also:

```bash
pip install pyinstaller
# pip install wheel==0.37.1 # could be needed
```

## 2.1. Run
Now you are able to run the code (being in the virtual environment)

```bash
python3 prog_raw.py
```

## 2.2 Bundle
Or you can bundle it for your Linux distribution

```bash
pyinstaller prog_raw.py --onefile --hidden-import=plyer.platforms.linux.filechooser --hidden-import=plyer.platforms.linux.notification --name SYNCmoss --icon icon_r.ico
```

copy from folder "dist" bundled file ("SYNCmoss" should be the only file there) to the folder with other software files (pictures, text...)
and most probably you will need also to do:

```bash
sudo chmod +x SYNCmoss
```

## 3. If it fails
There are too many Linux distributions and versions. If for your particular Linux destribution this manual does not work, you still can use anaconda to bundle the software: <br/>
1) please follow the manual "how_to_run_code.md" <br/>
2) at the end of "step2" do also:
```bash
pip install pyinstaller==4.7
```
3) then:
```bash
pyinstaller prog_raw.py --onefile --hidden-import=plyer.platforms.linux.filechooser --hidden-import=plyer.platforms.linux.notification --name SYNCmoss --icon icon_r.ico
```
4) copy from folder "dist" bundled file ("SYNCmoss" should be the only file there) to the folder with other software files (pictures, text...)
