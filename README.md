# SYNCmoss


## Description

SYNCmoss is software for fitting Mossbauer spectra measured with synchrotron Mossbauer source.	<br/>
However full functionality is available for conventional Mossbauer spectroscopy.	<br/>
Some simple models for time-domain is also supported and more is planed to be developed later. <br/>
02/11/2024 the new version based on python 3.12.7 and up-to-date libraries was released.

## References

Yaroslavtsev S., J. Synchrotron Rad., 2023	<br/>
[doi.org/10.1107/S1600577523001686](https://doi.org/10.1107/S1600577523001686)

gitlub link for download [https://gitlab.esrf.fr/yaroslav/syncmoss](https://gitlab.esrf.fr/yaroslav/syncmoss) (this site)

## Installation


For Windows no installation is required. Just download "Windows" folder and run *.exe file. 	<br/>
For Linux with python 3.8/3.9/3.10 download "source_code" folder and do:
```bash
sudo chmod +x start.sh
./start.sh
```
For MacOS or Linux without python 3.12 one _can try_ instruction "how_to_run_code.md". </br>
For bundling see "how_to_bundle.md".

## Requirements

Require OpenGL v2.1 or higher (basically it means that computer should be not older than from 2009) because GUI is based on Kivy v2.1.

## Compatability

Windows version is bundled on Windows 11 and checked on Windows 10/11. 	<br/>
If you use Linux, please use "start.sh" or "how_to_run_code.md"	<br/>

If software does not work on your Linux / Windows please contact [author](sergey.yaroslavtsev@esrf.fr)	<br/>
MacOS is not supported yet, however you _can try_ to run software through code see "how_to_run_code.md".

## Recommended

Screen resolution 1920x1080 or higher.	<br/>
Software demand on the CPU - the calculation time will directly depend on CPU capabilities.

## License

MIT license, see COPYING.txt

## Main features

- User-friendly graphical interface
- Can extract instrumental function from spectrum of standard absorber
- Sequence (batch) fitting 
- Simultaneous fitting
- Full Hamiltonian model for single crystal case
- 2-state relaxation model
- Many-state superparamagnetic relaxation model
- Anharmonic spin modulation (ASM) model
- Multi-dimensional distributions with correlations
- Expressions (which could be linked to parameters)
- Online (along with experiment) fitting
- Parallel calculations of full-transmission integral

## Requests/Suggestions

If you have idea how to make software better for users, you are welcome to write to [author](sergey.yaroslavtsev@esrf.fr) (sergey.yaroslavtsev@esrf.fr).	<br/>
If you can help with its implementation you are also welcome as it is open source project.

## Bug report
First please make sure that you are using the latest version of SYNCmoss.	<br/>
If you have found a reproducible bug please write to [author](sergey.yaroslavtsev@esrf.fr) (sergey.yaroslavtsev@esrf.fr). Email should contain the following:	<br/>
- OS information
- description of bug (crush/freeze/...)
- step-by-step procedure to achieve this bug
- if it happens only with specific files please attach them (spectrum, model, calibration, etc.)

It will be helpful if you can indicate specific error; for this you need to do the following:
- for Windows download "SYNCmoss_console" executable file (if you skip it before): for Linux just use main executable file "SYNCmoss"
- run this file through console (see details in Tips.pptx)
- reproduce the error
- copy text from console or print-screen it (make window as large as possible so error should be visiable - "Error:....")
- attach text-log file or screenshot to email

## Code structure
There are 6 main python files.
The one which contain GUI (prog_raw.py) has "spaghetti code". <br/>
However other files have reasonable names and structure, and they are easy to modify if needed. <br/>
For example to add a new model if the algorithm is known is a rather simple task.

## Author

Yaroslavtsev Sergey (ESRF ID14)

## Acknowledgements

Many thanks to: 	<br/>
Aprilis Georgios, Rosenthal Anja, Xiang Li, Girani Alice for beta-testing,  	<br/>
Chumakov Alexander and Aprilis Georgios for the advice and suggestions  	<br/>
Lopushenko Ivan for discussion about combined interaction for polarized radiation ("Hamilton_mc" model),  	<br/>
Silkov Alexander for help with GUI at initial stage of the project, and for help with Linux version 	<br/>
Retegan Marius and De Nolf Wout for implementation software on the beamline



